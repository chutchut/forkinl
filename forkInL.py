import sys
import os
import argparse

import requests
from bs4 import BeautifulSoup

#
# Scrape GitHub project forks for conditions indicating
# potential vulnerabilities
#
# Install: pip3 install requests bs4 lxml
#

PROJ_ROOT = os.getcwd() + '/projects'
GH_SEARCH = 'https://github.com/search?q='
GH_FORKS = 'https://github.com/%s/network/members'
GH_RAW = 'https://raw.githubusercontent.com/%s/master/%s'
REQ_HDR = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0'
}


def getFileList(projPath):
    fList = []
    for path, subdirs, files in os.walk(projPath):
        for name in files:
            fpath = '%s/%s' % (path, name)
            # Strip project path to get relative GitHub path
            fpath = fpath.replace(projPath + '/', '')
            fList.append(fpath)
    return fList


def getFileContent(path):
    try:
        f = open(path, 'r')
        c = f.read()
        f.close()
        return c
    except IOError as ioe:
        print('IOError: %s' % ioe)
        return ''


def getRawUrlContent(fork, file):
    try:
        forkName = '/'.join(fork)
        req = requests.get(GH_RAW % (forkName, file), headers=REQ_HDR)
        if req.status_code != 200:
            # Couldnt find the file in the fork
            return None
        return req.text.encode('utf-8')
    except Exception as e:
        print('Exception: %s' % e)
        return None


def getForks(project):
    print('Getting forks of project %s..' % project)
    # Attempt GitHub search
    sReq = requests.get(GH_SEARCH + project, headers=REQ_HDR)
    soup = BeautifulSoup(sReq.text.encode('utf-8'), features='lxml')
    repoList = soup.find('ul', class_='repo-list')
    parentProjHdr = repoList.find('h3')
    if not parentProjHdr:
        print('Couldnt find root project')
        sys.exit(1)
    parentProj = parentProjHdr.text.strip()
    print('Parent project is: %s (%s)' % (parentProj, 'https://github.com/' + parentProj))
    # Get forks of root project
    fReq = requests.get(GH_FORKS % parentProj, headers=REQ_HDR)
    soup = BeautifulSoup(fReq.text.encode('utf-8'), features='lxml')
    netDiv = soup.find('div', id='network')
    repoDivs = netDiv.find_all('div')
    repoForks = []
    for rdiv in repoDivs:
        if not rdiv.text:
            continue
        # Split on / and strip
        rdivSplit = rdiv.text.split('/')
        repoForks.append((rdivSplit[0].strip(), rdivSplit[1].strip()))
    print('Found %d forks' % (len(repoForks) - 1))
    return repoForks


def showConditions(cond):
    print('Conditions:')
    for key, item in cond.items():
        if item['content']:
            print('Check for file: %s, with content: %s' % (key, item['content']))
        else:
            print('Check for file: %s' % key)


def checkConditions(cond, forks, op):
    condMetForks = []
    for fork in forks:
        condStatList = []
        fileList = []
        for file, data in cond.items():
            fcont = getRawUrlContent(fork, file)
            condStat = False
            if fcont is not None:
                # File exists
                if not data['content']:
                    condStat = True
                elif data['content'].encode('utf-8') in fcont:
                    # Content exists
                    condStat = True
                if condStat:
                    fileList.append(file)
            condStatList.append(condStat)
        # AND / OR
        if (op.lower() == 'or' and any(condStatList)) or (op.lower() == 'and' and all(condStatList)):
            condMetForks.append((fork, fileList))
    return condMetForks


def run(args):
    proj = args.project
    conds = {}
    projPath = PROJ_ROOT + '/' + proj
    # Get files recursively and their content to determine conditions
    projFiles = getFileList(projPath)
    if args.match and args.match not in projFiles:
        print('Invalid match: %s' % args.match)
        sys.exit(1)
    elif args.match and args.match in projFiles:
        projFiles = [args.match]
    for pFile in projFiles:
        pFilePath = projPath + '/' + pFile
        conds[pFile] = {'content': getFileContent(pFilePath)}
    if not conds:
        print('No conditions!')
        return
    showConditions(conds)
    forks = getForks(proj)
    if not forks:
        print('No forks!')
        return
    # Parent fork at index 0
    pfork = forks[0]
    print('Checking repos master branch..')
    filterForks = checkConditions(conds, forks, args.op)
    print('Found %d repos which met conditions' % len(filterForks))
    for fork in filterForks:
        if fork[0] is pfork:
            key = 'Parent'
        else:
            key = 'Fork'
        forkName = '/'.join(fork[0])
        forkFiles = fork[1]
        print('%s: %s (%s) Matched: %s' % (key, forkName, 'https://github.com/' + forkName, forkFiles))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--project', help="Source GitHub project (must exist in project path: %s)" % PROJ_ROOT)
    parser.add_argument('--op', help="Condition operator (AND/OR), default: OR")
    parser.add_argument('--match', help="Specific match (invalidates --op)")
    print('# forkInL - Scrape GitHub project forks for conditions indicating potential vulnerabilities')
    print('# 0.1 - chutchut\n')
    args = parser.parse_args()
    if not args.project:
        print('Missing arg')
    elif args.project not in os.listdir(PROJ_ROOT):
        print('Invalid project: %s' % args.project)
    else:
        if not args.op:
            args.op = 'OR'
        print('Project: %s' % args.project)
        print('Operation: %s' % args.op)
        run(args)
